# Entrega practica

## Datos

* Nombre: Alejandro Gallardo Fontanet
* Titulación: Grado en Ingeniería en Sistemas de Telecomunicación
* Despliegue (url): http://nanofet.pythonanywhere.com/miscosas/
* Video básico (url): https://www.youtube.com/watch?v=35oBBd2rwi8

## Cuenta Admin Site

* nanofet/realbetis

## Cuentas usuarios

* pepe/pepe
* juan/juan
* montse/montse
* alex/betis

## Resumen parte obligatoria


- Funcionamiento

Se trata de una aplicación web que nos permite gestionar el contenido de otras páginas web, concretamente de YouTube y de Lastfm.

- Uso de la aplicación

A continuación se detallan las funciones de cada de una de las páginas por las que el usuario puede navegar:
Página principal

Al comienzo se ofrece al usuario la posibilidad de iniciar sesión o de registrarse, si así lo desea.

En esta página se muestra el TOP 10 de items más votados en la aplicación. También encontraremos la posibilidad de rellenar unos formularios para seleccionar el alimentador que nos interese. Para el caso de YouTube se introducirá el id del canal y, en el caso de Lastfm, deberemos introducir el nombre del artista.

Seguidamente se muestra un pequeño historial de los alimentadores visitados más recientemente. Estos alimentadores podrán ser eliminados por cualquier usuario, esté registrado o no, sin ser eliminados de la base de datos.

Se ofrece al usuario la posibilidad de iniciar sesión o de registrarse, si así lo desea.

- Página Alimentadores

En esta página veremos los alimentadores que han sido seleccionados en cualquier momento, ordenados por origen de alimentador, es decir, si viene de Youtube o de Last.fm

- Página Alimentador

En esta página se detallan los ítems que contiene dicho de alimentador, dando la posibilidad de acceder a la página de dicho ítem para obtener más información.
Página Item

Aquí se muestran los detalles del ítem que estamos investigando, se mostrará el video empotrado o la portada del álbum, dependiendo de si se trata de un ítem de Youtube o Last.fm. Se dará la posibilidad al usuario de decidir si le gusta o no le gusta dicho contenido, así como un contador de los votos recibidos por ese ítem hasta el momento

- Página Usuarios

Para finalizar, en esta página se mostrarán los usuarios registrados junto con los items votados y comentados.



