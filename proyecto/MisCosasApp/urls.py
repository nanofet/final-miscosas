from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('informacion/', views.informacion, name='informacion'),
    path('usuarios/', views.usuarios, name='usuarios'),
    path('alimentadores/', views.alimentadores, name='alimentadores'),
    path('registro/', views.registro, name='registro'),
    path('usr_logout/', views.usr_logout, name='usr_logout'),
    path('error404/', views.error_404, name='error_404'),
    path('<str:llave1>/<str:llave2>', views.proceso_alimentador, name= 'proceso_alimentador'),
    path('<str:llave>/<str:llave1>/Deseleccionar', views.borrar_alimentador, name= 'borrar_alimentador'),
    path('<str:llave>/<str:llave1>/<str:llave2>', views.proceso_item, name= 'proceso_item'),
    path('usuario/<str:llave>',views.usuario, name='usuario'),
    path('<str:llave>/<str:llave1>/<str:llave2>/<str:llave3>/votopositivo/', views.votos, name='votos'),
    path('<str:llave>/<str:llave1>/<str:llave2>/<str:llave3>/votonegativo/', views.votos, name='votos'),
]