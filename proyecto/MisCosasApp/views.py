import sys

from django.shortcuts import render, redirect
from .forms import FormularioRegistro, FormularioLogin
from .models import Users, Alimentador, Item, Voto, Comentario
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect, HttpResponseForbidden
import urllib.request
from xml.sax import make_parser
from .parseyt import YTHandler
from .apikeys import LASTFM_APIKEY
from .parselastfm import LastfmHandler
from unidecode import unidecode


def index(request): # CREO QUE ESTÁ TERMINADO, NO ESTOY SEGURO
    paginaPrincipal = True
    form = FormularioLogin()
    context = {'form': form}
    if request.method == "POST":
        form = FormularioLogin(request.POST, request.FILES)
        if form.is_valid():
            nombre = form.cleaned_data['Usuario']
            contraseña = form.cleaned_data['Contraseña']
            user = authenticate(request, username=nombre, password=contraseña)
            if user is not None:
                login(request, user)
                return redirect("index")
            else:
                return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

    if request.method == "POST":
        action = request.POST.get('action',False)
        if action == "youtube":
            idchannel = request.POST['idyoutube']
            redireccion = "/miscosas/youtube/" + idchannel
            return HttpResponseRedirect(redireccion)

        elif action == "lastfm":
            artist = request.POST['artistlast']
            artist = unidecode(artist)
            redireccion = "/miscosas/lastfm/" + artist
            return HttpResponseRedirect(redireccion)

    alimentadoresYT = Alimentador.objects.filter(pagPrincipal=True, type="youtube")
    alimentadoresLF = Alimentador.objects.filter(pagPrincipal=True, type="lastfm")

    items =Item.objects.all()
    for item in items:
        item.votosTotales = item.votosPositivos - item.votosNegativos
        item.save()

    itemsOrdenados = Item.objects.all().order_by('-votosTotales')[:10]

    context = {'paginaPrincipal': paginaPrincipal,
               'form': form,
               'alimentadoresYT': alimentadoresYT,
               'alimentadoresLF': alimentadoresLF,
               'top10': itemsOrdenados}

    user = request.user.username
    if user:
        try:
            user = Users.objects.get(username=user)
        except Users.DoesNotExist:
            user = Users(username=user)
            user.save()
        votos = Voto.objects.filter(usuario=user).order_by('-id')[:5]
        context['itemsvotados'] = votos

    return render(request, 'MisCosasApp/index.html', context)

def informacion(request): # TERMINADO
    return render(request, 'MisCosasApp/informacion.html')

def usuarios(request): # TERMINADO
    users = Users.objects.all()
    context = {'users': users}

    return render(request,'MisCosasApp/usuarios.html', context)


def alimentadores(request): # TERMINADO
    try:
        alimentadoresYT = Alimentador.objects.filter(type="youtube")
        alimentadoresLF = Alimentador.objects.filter(type="lastfm")

        context = {'alimentadoryoutube': alimentadoresYT,
                   'alimentadoreslast': alimentadoresLF}

    except Alimentador.DoesNotExist:
        context = {}
    return render(request, 'MisCosasApp/alimentadores.html',context)

def registro(request): # TERMINADO
    form = FormularioRegistro()
    context = {'form': form}
    if request.method == "POST":
        form = FormularioRegistro(request.POST, request.FILES)
        if form.is_valid():
            nombre = form.cleaned_data['Usuario']
            email = form.cleaned_data['Email']
            contraseña = form.cleaned_data['Contraseña']
            try:
                superUser = User.objects.get(username=nombre)
                context['error'] = 'Usuario existente, pruebe otro'
                return render(request, 'MisCosasApp/registro.html', context)
            except User.DoesNotExist:
                try:
                    user = Users.objects.get(username=nombre)
                    context['error'] = 'Usuario existente, pruebe otro'
                    return render(request, 'MisCosasApp/registro.html', context)
                except Users.DoesNotExist:
                    u = Users(username=nombre, email=email, password=contraseña)
                    u.save()
                    user = User.objects.create_user(username=nombre, email=email, password=contraseña)
                    user.save()
                    if user is not None:
                        login(request, user)
                    return render(request, 'MisCosasApp/index.html', context)

    return render(request, 'MisCosasApp/registro.html', context)

def usr_logout(request): #TERMINADO
    logout(request)
    return redirect(index)

def error_404(request): # TERMINADO
    return render(request, 'MisCosasApp/error404.html')

def proceso_alimentador(request, llave1, llave2): # TERMINADO
    Parser = make_parser()
    if llave1 == 'youtube':
        Parser.setContentHandler(YTHandler())
        url = 'https://www.youtube.com/feeds/videos.xml?channel_id=' \
            + llave2
    if llave1 == 'lastfm':
        llavetraducida = llave2.replace(" ", "%20")
        Parser.setContentHandler(LastfmHandler())
        url = 'http://ws.audioscrobbler.com/2.0/?method=artist.gettopalbums&artist=' + llavetraducida + '&api_key=' + LASTFM_APIKEY
    try:
        xmlStream = urllib.request.urlopen(url)
        Parser.parse(xmlStream)
    except:
        return render(request, 'MisCosasApp/error404.html', {})
    try:
        a = Alimentador.objects.get(alimentadorId=llave2)
    except:
        return render(request, 'MisCosasApp/error404.html', {})

    items = Item.objects.filter(alimentador=a)

    for item in items:
        item.votosTotales = item.votosPositivos - item.votosNegativos
        item.save()

    context = {'contentList': items,
               'alimentador': a}

    return render(request, 'MisCosasApp/alimentador.html', context)

def proceso_item(request, llave, llave1, llave2): # TERMINADO
    a = Alimentador.objects.get(alimentadorId=llave1)
    i = Item.objects.get(itemId=llave2)

    context = {'alimentador': a,
               'item': i}

    return render(request, 'MisCosasApp/item.html', context)

def borrar_alimentador(llave, llave1): # ERROR!!!!!!!!!!!!!!!!!

    a = Alimentador.objects.get(type=llave, alimentadorId=llave1)
    a.pagPrincipal = not a.pagPrincipal
    a.save()

    return redirect('index')

def usuario(request, llave):
    users = Users.objects.get(username=llave)
    itemsComentados = Comentario.objects.filter(usuario=users)
    itemsVotados = Voto.objects.filter(usuario=users)
    context = {'users': users,
               'itemscomentados': itemsComentados,
               'itemsvotados': itemsVotados,
               }

    if users.username == request.user.username:
        sameuser = True
        context['sameuser'] = sameuser

    return render(request, 'MisCosasApp/usuario.html', context)

def votos(request, llave, llave1, llave2, llave3):
    i = Item.objects.get(itemId=llave2)
    name = request.user.username
    if name:
        action = request.POST.get('action', None)
        u = Users.objects.get(username=name)

        if action == "votopositivo":
            try:
                v = Voto.objects.get(usuario=u, item=i)
                if v.estado == "estadonegativo":
                    i.votosPositivos = i.votosPositivos + 1
                    i.votosNegativos = i.votosNegativos - 1
                    i.save()
                v.estado = "estadopositivo"
                v.save()
            except Voto.DoesNotExist:
                i.votosPositivos = i.votosPositivos + 1
                i.save()
                v = Voto(usuario=u, item=i, estado="estadopositivo")
                v.save()

        elif action == "votonegativo":
            try:
                v = Voto.objects.get(usuario=u, item=i)
                if v.estado == "estadopositivo":
                    i.votosPositivos = i.votosPositivos - 1
                    i.votosNegativos = i.votosNegativos + 1
                    i.save()
                v.estado = "estadonegativo"
                v.save()
            except Voto.DoesNotExist:
                i.votosNegativos = i.votosNegativos + 1
                i.save()
                v = Voto(usuario=u, item=i, estado="estadonegativo")
                v.save()

            a = Alimentador.objects.get(alimentadorId=llave1)
            items = Item.objects.filter(alimentador=a)
            votospos = 0
            votosne = 0
            for votos in items:
                votospos = votos.votosPositivos + votospos
                votosne = votos.votosNegativos + votosne

            puntuacionAlimentador = votospos - votosne
            a.puntuacion = puntuacionAlimentador
            a.save()
        if llave3 == 'paginaPrincipal':
            redireccion = "/miscosas/"
        elif llave3 == 'paginaItem':
            redireccion = "/miscosas" + "/" + llave + "/" + llave1 + "/" + llave2

        return redirect(redireccion)

