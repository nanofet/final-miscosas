from django import forms

class FormularioRegistro(forms.Form):
    Usuario= forms.CharField(widget=forms.TextInput(attrs={
        'placeholder': 'Introduzca un usuario'}))
    Email= forms.EmailField(widget=forms.TextInput(attrs={
        'placeholder': 'Introduzca su email'}))
    Contraseña= forms.CharField(max_length=32, widget=forms.PasswordInput)


class FormularioLogin(forms.Form):
    Usuario= forms.CharField(widget=forms.TextInput(attrs={
        'placeholder': 'Introduzca su usuario'}))
    Contraseña= forms.CharField(max_length=32, widget=forms.PasswordInput)


