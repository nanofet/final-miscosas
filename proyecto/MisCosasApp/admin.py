from django.contrib import admin

from .models import Users, Alimentador, Item, Voto, Comentario

admin.site.register(Users)
admin.site.register(Alimentador)
admin.site.register(Item)
admin.site.register(Voto)
admin.site.register(Comentario)